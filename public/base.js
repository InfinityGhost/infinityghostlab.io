const input = document.getElementById("in")
const output = document.getElementById("out")

const encode = document.getElementById("en")
const decode = document.getElementById("de")

const copy = document.getElementById("cp")

input.value = ""
output.value = ""

encode.onclick = (ev) => {
    output.value = btoa(input.value)
}

decode.onclick = (ev) => {
    output.value = atob(input.value)
}

const clip = new ClipboardJS('#cp')